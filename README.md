# hq.akdev.xyz: Infrastructure As Code

This repository aims to collect the configuration of my entire home network, otherwise known as hq.akdev.xyz

![diagrams/network.png](diagrams/network.png)

## Deploymment dependencies

The following tools are used to manage configuration across the environment

1. terraform
1. libvirtd
1. ansible

At the time of this writing the terraform state has been moved onto the gitlab instance, meaning the environment is partly self-hosting.

A script is offered under [ci/install-deps.sh](ci/install-desp.sh) that will install the dependencies needed. It assumes a Fedora host.

## Services offered

1. Plex Media Server
1. QBittorrent server behind NordVPN
1. GitLab instance + podman runner
1. Sonarr
1. NFS

## Virtualization services

Most services run on virtualized hosts however a few are running on baremetal hosts as they were required for bootstrapping and/or are required to maintain the virtual environment.

1. NFS
1. openvswitch
1. ovn-controller
1. ovn-host

## How to deploy

From cli execute: `make deploy` to deploy both the terraform changes and ansible playbooks.

You can also use the different make subcommands to manage the environment:

```
$ make ansible-check  # run a --check
$ make deploy-ansible # deploy only the ansible playbooks (root is main.yml)
$ make tf-apply       # apply terraform state (will ask for confirmation as usual)
$ make tf-plan        # terraform plan
$ make tf-apply-auto  # apply terraform state without confirmation, intended for ci usage (dangerous)
```
