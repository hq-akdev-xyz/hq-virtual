#!/bin/bash

mkdir -p ~/.ssh
ln -sv "$GITLAB_SSH_KEY" ~/.ssh/id_rsa
cat > ~/.ssh/known_hosts <<EOF
virt0.hq.akdev.xyz ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBLROhjmYAJD8z6T439jrlxKLYJkVu3gwm+BZdIBAyJHdrn69WDociztrVGiya26T3Tfnz0IsikAe21dvnN7TtOw=
EOF
