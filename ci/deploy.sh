#!/bin/bash

set -xeu


(
    cd terraform

    terraform init
    terraform apply -auto-approve 
)

ansible-playbook main.yml
