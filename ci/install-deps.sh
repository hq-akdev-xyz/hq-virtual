#!/bin/bash

set -xeu

curl "https://rpm.releases.hashicorp.com/fedora/hashicorp.repo" \
    -Lo /etc/yum.repos.d/hashicorp.repo

dnf install -y terraform ansible openssh-clients genisoimage
