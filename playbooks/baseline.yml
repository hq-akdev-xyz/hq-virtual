---
- name: configure baseline
  hosts: all
  handlers:
    - name: refresh kernel options
      shell: sysctl --system
      listen: kernel_reload
    - name: sshd configuration reload
      systemd:
        name: sshd
        state: reloaded
      listen: ssh_reload
    - name: modprobe.d configuration reload
      systemd:
        name: systemd-modules-load
        state: restarted
      listen: modprobe_reload
  tasks:
    - name: set selinux to permissive (temporary)
      selinux:
        policy: targeted
        state: permissive
    - name: remove unnecessary packages
      yum:
        name:
          - at
        state: absent
    - name: install packages for CentOS
      when: 'ansible_distribution != "Fedora"'
      yum:
        state: present
        name:
          - epel-release
    - name: install common packages
      yum:
        state: present
        name:
          - dnf-automatic
          - rkhunter
          - kitty-terminfo
    - name: enable dnf-automatic
      systemd:
        name: dnf-automatic-install.timer
        enabled: yes
    - name: lock down permissions at/cron configuration files
      file:
        path: "{{item}}"
        mode: 'u=rwX,g=---,o=---'
      loop:
        - /etc/cron.deny
        - /etc/crontab
        - /etc/issue
        - /etc/issue.net
        - /etc/cron.d
        - /etc/cron.daily
        - /etc/cron.hourly
        - /etc/cron.weekly
        - /etc/cron.monthly
    - name: harden kernel parameters
      copy:
        src: system/sysctl/50-default.conf
        dest: /etc/sysctl.d/50-default.conf
        mode: '0600'
      notify: kernel_reload
    - name: harden sshd daemon
      copy:
        src: system/sshd_config
        dest: /etc/ssh/sshd_config
        mode: '0600'
      notify: ssh_reload
    - name: blacklist unnecessary kernel modules
      copy:
        src: system/modprobe.d/00-baseline-blacklist.conf
        dest: /etc/modprobe.d/00-baseline-blacklist.conf
        mode: '0600'
      notify: modprobe_reload
    - name: configure /etc/login.defs
      copy:
        src: system/login.defs
        dest: /etc/login.defs
        mode: '0644'
    - name: configure /etc/security/limits.conf
      copy:
        src: system/security/limits.conf
        dest: /etc/security/limits.conf
        mode: '0644'
    - name: set authorized ssh keys
      authorized_key:
        user: akdev
        state: present
        key: "{{lookup('file', item)}}"
      loop:
        - system/akdev/yubikey-ssh.pub
        - system/akdev/canzuk-ssh.pub
        - system/akdev/mobile-ssh.pub
    - name: configure dnf
      copy:
        src: system/dnf.conf
        dest: /etc/dnf/dnf.conf

#- name: configure storage for guests
#  hosts: libvirt_domains
#  tasks:
#    - name: configure storage mountpoint
#      mount:
#        src: virt0.hq.akdev.xyz:/var/lib/storage/guests/shared
#        path: /var/lib/storage
#        fstype: nfs
#        state: mounted
#        opts: _netdev,noauto,x-systemd-automount,x-systemd-device-timeout=1

