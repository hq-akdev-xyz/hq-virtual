data "template_file" "sandbox_user_data" {
    template = file("${path.module}/files/cloudinit.cfg")
    vars = {
        hostname = "sandbox.hq.akdev.xyz"
    }
}

resource "libvirt_cloudinit_disk" "sandbox_cloudinit_disk" {
    provider = libvirt.virt0
    name = "sandbox_cloudinit.iso"
    user_data = data.template_file.sandbox_user_data.rendered
}
resource "libvirt_volume" "sandbox_root" {
    provider = libvirt.virt0
    name = "sandbox.hq.akdev.xyz"
    format = "qcow2"
    pool = "guest-storage"
    source = "https://repo.almalinux.org/almalinux/8/cloud/x86_64/images/AlmaLinux-8-GenericCloud-latest.x86_64.qcow2"
}

resource "libvirt_domain" "sandbox_domain" {
    provider = libvirt.virt0
    name = "sandbox.hq.akdev.xyz"
    memory = 4096
    cloudinit = libvirt_cloudinit_disk.sandbox_cloudinit_disk.id
    vcpu = 2
    disk {
        volume_id = libvirt_volume.sandbox_root.id
    }
    network_interface {
        network_name = "ovn-dmz"
    }
    xml {
        xslt = file("files/libvirt/vlan-7.xslt")
    }
}
