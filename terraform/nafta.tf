data "template_file" "nafta_user_data" {
    template = file("${path.module}/files/windows/Autounattend.xml")
}

resource "libvirt_volume" "windows_installer" {
    provider = libvirt.canzuk
    name = "windows.iso"
    format = "iso"
    pool = "workshop.libvirt.images"
    source = "https://software-download.microsoft.com/pr/Win10_21H2_English_x64.iso?t=466883a9-3fed-4d06-ace1-8b2d64a52d54&e=1640494837&h=44f70ba8feb8b840e135a001cfe300b0"
}
resource "libvirt_volume" "virtio_win" {
    provider = libvirt.canzuk
    source = "https://software-download.microsoft.com/pr/Win10_21H2_English_x64.iso?t=87a068c6-44a3-4706-bf0e-2550f2e384a8&e=1640505408&h=7ea7a8875b8d7d95cdbade8ce13ba1d4"
    name = "virtio-win.iso"
    format = "iso"
    pool = "workshop.libvirt.images"
} 
resource "libvirt_volume" "nafta_root" {
    provider = libvirt.canzuk
    name = "nafta.hq.akdev.xyz"
    format = "qcow2"
    pool = "workshop.libvirt.images"
    size = 107374182400
}

resource "libvirt_domain" "nafta_domain" {
    provider = libvirt.canzuk
    name = "nafta.hq.akdev.xyz"
    memory = 8192
    vcpu = 8
    running = false
    disk { volume_id = libvirt_volume.nafta_root.id }
    firmware = "/usr/share/OVMF/OVMF_CODE.fd"
    network_interface {
        network_name = "ovn-dmz"
        mac = "02:01:01:01:01:01"
    }
    xml {
        xslt = file("files/libvirt/nafta.xslt")
    }

    depends_on = [libvirt_domain.otan_domain]
}
