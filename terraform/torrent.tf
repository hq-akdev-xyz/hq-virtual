data "template_file" "torrent_user_data" {
    template = file("${path.module}/files/cloudinit.cfg")
    vars = {
        hostname = "torrent.hq.akdev.xyz"
    }
}

resource "libvirt_cloudinit_disk" "torrent_cloudinit_disk" {
    provider = libvirt.virt0
    name = "torrent_cloudinit.iso"
    user_data = data.template_file.torrent_user_data.rendered
}
resource "libvirt_volume" "torrent_root" {
    provider = libvirt.virt0
    name = "torrent.hq.akdev.xyz"
    format = "qcow2"
    pool = "guest-storage"
    source = "https://repo.almalinux.org/almalinux/8/cloud/x86_64/images/AlmaLinux-8-GenericCloud-latest.x86_64.qcow2"
}

resource "libvirt_domain" "torrent_domain" {
    provider = libvirt.virt0
    name = "torrent.hq.akdev.xyz"
    memory = 2048
    cloudinit = libvirt_cloudinit_disk.torrent_cloudinit_disk.id
    disk {
        volume_id = libvirt_volume.torrent_root.id
    }
    network_interface {
        network_name = "ovn-dmz"
    }
    xml {
        xslt = file("files/libvirt/vlan-7.xslt")
    }
}
