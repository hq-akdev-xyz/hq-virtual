data "template_file" "plex_user_data" {
    template = file("${path.module}/files/cloudinit.cfg")
    vars = {
        hostname = "plex.hq.akdev.xyz"
    }
}

resource "libvirt_cloudinit_disk" "plex_cloudinit_disk" {
    provider = libvirt.virt0
    name = "plex_cloudinit.iso"
    user_data = data.template_file.plex_user_data.rendered
}

resource "libvirt_volume" "plex_root" {
    provider = libvirt.virt0
    name = "plex.hq.akdev.xyz"
    format = "qcow2"
    pool = "guest-storage"
    source = "https://repo.almalinux.org/almalinux/8/cloud/x86_64/images/AlmaLinux-8-GenericCloud-latest.x86_64.qcow2"
}

resource "libvirt_domain" "plex_domain" {
    provider = libvirt.virt0
    name = "plex.hq.akdev.xyz"
    memory = 2048
    cloudinit = libvirt_cloudinit_disk.plex_cloudinit_disk.id
    disk {
        volume_id = libvirt_volume.plex_root.id
    }
    network_interface {
        network_name = "ovn-dmz"
    }
    xml {
        xslt = file("files/libvirt/vlan-7.xslt")
    }
}
