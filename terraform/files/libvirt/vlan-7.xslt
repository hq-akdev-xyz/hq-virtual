<?xml version="1.0" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes" indent="yes"/>
  <!-- Identity copy -->
  <xsl:template match="node()|@*">
     <xsl:copy>
       <xsl:apply-templates select="node()|@*"/>
     </xsl:copy>
  </xsl:template>

  <!-- Add portgroup="vlan-7" -->
  <xsl:template match="/domain/devices/interface[@type='network']/source[@network='ovn-dmz']">
     <xsl:copy>
        <xsl:apply-templates select="@*|node()" />
       <xsl:attribute name="portgroup">vlan-7</xsl:attribute>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
