<?xml version="1.0" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes" indent="yes"/>
  <!-- Identity copy -->
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  <!-- Match forward mode -->
  <xsl:template match="/network/forward">
    <!-- Copy forward mode -->
    <xsl:copy-of select="." />

    <!-- Insert openvswitch port type -->
    <xsl:element name="virtualport">
      <xsl:attribute name="type">openvswitch</xsl:attribute>
    </xsl:element>

    <!-- Insert VLAN XML -->
    <portgroup name="vlan-1">
    </portgroup>
    <portgroup name="vlan-3">
      <vlan>
        <tag id="3"/>
      </vlan>
    </portgroup>
    <portgroup name="vlan-5" default="yes">
      <vlan>
        <tag id="5"/>
      </vlan>
    </portgroup>
    <portgroup name="vlan-7">
      <vlan>
        <tag id="7"/>
      </vlan>
    </portgroup>
    <portgroup name="dmz-11">
      <vlan>
        <tag id="11"/>
      </vlan>
    </portgroup>
    <portgroup name="vlan-all">
      <vlan trunk="yes">
        <tag id="3"/>
        <tag id="5"/>
        <tag id="7"/>
        <tag id="11"/>
      </vlan>
    </portgroup>
  </xsl:template>
</xsl:stylesheet>
