deploy: tf-apply ansible-deploy
check: tf-plan ansible-check

tf-plan:
	terraform -chdir=terraform plan
tf-apply:
	terraform -chdir=terraform apply -auto-approve
tf-apply-auto:
	terraform -chdir=terraform apply -auto-approve

ansible-deploy:
	ansible-playbook main.yml -e @ansible-vault.yml \
		 --ask-vault-pass

ansible-check:
	ansible-playbook main.yml -e @ansible-vault.yml \
		--ask-vault-pass --check

ci-get-creds:
	./ci/get-creds.sh
